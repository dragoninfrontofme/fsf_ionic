angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope, $cordovaSocialSharing, $cordovaImagePicker, $cordovaCamera) {

  $scope.getPicture = function(){
    var options = {
        maximumImagesCount: 1,
        width: 800,
        height: 800,
        quality: 80
    };

    $cordovaImagePicker.getPictures(options)
    .then(function (results) {
      if(results.length > 0){
        alert('Image URI: ' + results[0]);
      }
    }, function(error) {
      // error getting photos
    });
  }

  $scope.takePicture = function(){
    var options = {
      destinationType: Camera.DestinationType.FILE_URI,
      sourceType: Camera.PictureSourceType.CAMERA,
    };  

    $cordovaCamera.getPicture(options).then(function(imageURI) {
      alert(imageURI);

      //assign to image:

    }, function(err) {
      // error do something
    });
  }

  $scope.facebookLoginError = function(response){
    alert("Error: " + response);
  }

  $scope.facebookLoginSuccess = function(response){
    alert("Successfully login!");
    //post to server
  }

  $scope.loginFacebook = function(){
    facebookConnectPlugin.getLoginStatus(function(success){
      console.log("Debug Success: " + success);
      if(success.status === 'connected'){
        //is already connected / user allow login via the app
        alert('Success, not first time login');
      }else{
        //firsttime user access the app:
        facebookConnectPlugin.login(['email', 'public_profile'], 
          $scope.facebookLoginSuccess, 
          $scope.facebookLoginError);
      }
    })
  }

  $scope.share = function(){
    $cordovaSocialSharing
    .share(null, null, null, "http://google.com")
    // share(message, subject, file, link)
    .then(function(result) {
    }, function(err) {
      //do something
    });
  }

})
.controller('ChatsCtrl', function($scope, Chats) {
  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
});
