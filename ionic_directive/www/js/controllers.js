angular.module("starter")
.controller('ControllerOne', ControllerOne)
.controller('ControllerTwo', ControllerTwo);

ControllerOne.$inject = ["$ionicActionSheet", "$scope", "$http", "$ionicLoading", "$timeout", "MyModal", "$ionicPopup"];

function ControllerOne($ionicActionSheet, $scope, $http, $ionicLoading, $timeout, MyModal, $ionicPopup){
	var vm = this;

	vm.items = [1,2,3,4,5,6,7,8,9,10];

	vm.showModal = function(){
		MyModal
		.init(vm)
		.then(function(modal){
			modal.show();
		})
	}

	vm.showPopUp = function(){
		var confirmPopup = $ionicPopup.confirm({
			title: 'Confirmation',
			template: 'Are you sure?'
		});

		confirmPopup.then(function(res) {
			if(res) {
				console.log('You are sure');
			} else {
				console.log('You are not sure');
			}
		});
	}

	vm.doRefresh = function(){
		console.log("Get some data from server");

		$http({
	      method: 'GET',
	      url: "http://demo5964607.mockable.io/login",
	      data: {},
	      headers: {
	       'Content-Type': "application/json"
	      },
	    }).then(function successCallback(response){
	    	$scope.$broadcast('scroll.refreshComplete');
	    }, function errorCallback(response) {
	    	$scope.$broadcast('scroll.refreshComplete');
	    });
	}

	vm.showLoader = function(){
		$ionicLoading.show({
			template: '<ion-spinner icon="android"></ion-spinner>'
		});

		$timeout(function(){
			$ionicLoading.hide();
		}, 4000);
	}

	vm.showActionSheet = function(){
		$ionicActionSheet.show({
			buttons: [
				{ text: 'Button 1' },
				{ text: 'Button 2' }
			],
			destructiveText: 'Delete',
			titleText: 'Share',
			cancelText: 'Cancel',
			cancel: function(){
				//do something 
			},
			buttonClicked: function(index){
				if(index == 0){
					//do something
				}
				console.log(index);
				return true;
			},
			destructiveButtonClicked: function(){
				return true;
			}
		});
	}
}


ControllerTwo.$inject = ["$scope"];

function ControllerTwo($scope){
	var vm = this;
	vm.myList = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];
	vm.loadStop = true;

	vm.loadMoreItems = function(){
		console.log("test");
		var nextList = vm.myList.length + 1;
		vm.myList.push(nextList);
		if(vm.myList.length > 50){
			vm.loadStop = false;
		}

		$scope.$broadcast('scroll.infiniteScrollComplete');
	}

}
