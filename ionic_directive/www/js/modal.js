angular.module("starter.modal", [])
.factory('MyModal', function($ionicModal, $rootScope){

	var init = function(){
		$scope = $rootScope.$new();

		var mdl = $ionicModal.fromTemplateUrl('templates/my-modal.html', {
			scope: $scope,
      		animation: 'slide-in-up'
		}).then(function(modal){
			$scope.modal = modal;
			return modal;
		});

		return mdl;
	}

	return {
		init: init
	};
});